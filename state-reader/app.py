from influxdb import InfluxDBClient
import math
from urllib.request import urlopen # reading web page to string
from lxml import etree # parsing html table
import time # for regular execution
import json
import sys


def connect_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """

    influx_db = InfluxDBClient(host='influxdb', port=8086)
    influx_db.switch_database('influx')
    
    return influx_db

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """


# Map parsed HTML to InfluxDB JSON
def create_db_json_htmltable(el):

    json_body  = [
        {
            "measurement": "taisto",
            "tags": {
                "name": el["Torni"]
            },
            "fields": {
                "count": int(el["Lajimäärä"])
            }
        }
    ]

    return json_body


# Read state and store to database
def read_n_store_htmltable():

    # Get database connection
    influx_client = InfluxDBClient(host='influxdb', port=8086)
    influx_client.switch_database('influx')

    html = urlopen('https://www.biomi.org/taisto/tilanne/')
    s = html.read().decode('utf-8')
    print(s)

    table = etree.HTML(s).find("body/table")
    rows = iter(table)
    headers = [col.text for col in next(rows)]
    for row in rows:
        values = [col.text for col in row]
        print(dict(zip(headers, values)))
        db_json = create_db_json_htmltable(dict(zip(headers, values)))
        print(db_json)
        influx_client.write_points(db_json)

    # Close database connection
    influx_client.close()


# Map taisto JSON API to InfluxDB JSON
def create_db_json_taistoapi(el):

    json_body  = [
        {
            "measurement": "taisto",
            "tags": {
                "id": el["id"],
                "name": el["name"]
            },
            "fields": {
                "speciesCount": int(el["speciesCount"]),
                "button": el["button"],
                "updated": float(el["updated"]),
                "ip": el["ip"]
            }
        }
    ]

    return json_body

# Read state from JSON API and store to database
def read_n_store_taistoapi():

    # Get database connection
    influx_client = InfluxDBClient(host='influxdb', port=8086)
    influx_client.switch_database('influx')

    SOURCE_URL = 'https://www.biomi.org/taisto/?format=json'

    html = urlopen(SOURCE_URL)
    s = html.read().decode(encoding = 'utf8')
    s_json = json.loads(s)

    if isinstance(s_json, list):
        for value in s_json:
            #print(key)
            #print(value['name'])
            db_json = create_db_json_taistoapi(value)
            print(db_json)
            influx_client.write_points(db_json)

    if isinstance(s_json, dict):
        for key, value in s_json.items():
            #print(key)
            #print(value['name'])
            db_json = create_db_json_taistoapi(value)
            print(db_json)
            influx_client.write_points(db_json)


    # Close database connection
    influx_client.close()


if __name__ == '__main__':

    RUN_INTERVAL = 15; # seconds

    while True:
        try:
            read_n_store_taistoapi()
            #read_n_store_htmltable()
        except:
            exctype, value = sys.exc_info()[:2]
            log = 'Loop error: ' + str(value)
            print(log)
            #with open(LOGFILE, 'a') as file:
            #    file.write(log + '\n')

        time.sleep(RUN_INTERVAL)

